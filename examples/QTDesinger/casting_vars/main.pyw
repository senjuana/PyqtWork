import sys

from castingnumers import *

class MiFormulario(QtGui.QDialog):
    def __init__(self,parent=None ):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        QtCore.QObject.connect(self.ui.bsumar, QtCore.SIGNAL('clicked()'), self.mostrarsum)

    def mostrarsum(self):
        if len(self.ui.input1.text()) != 0:
            a = int(self.ui.input1.text())
        else:
            a = 0
        if len(self.ui.input2.text()) != 0:
            b = int(self.ui.input2.text())
        else:
            b = 0
        suma = a+b
        self.ui.ldisplay.setText("suma: "+str(suma))
        


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    myapp = MiFormulario()
    myapp.show()
    sys.exit(app.exec_())
    
