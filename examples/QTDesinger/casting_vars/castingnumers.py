# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'castingnumbers.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(400, 300)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(50, 50, 101, 20))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(40, 110, 121, 20))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(90, 150, 71, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.ldisplay = QtGui.QLabel(Dialog)
        self.ldisplay.setGeometry(QtCore.QRect(180, 150, 181, 17))
        self.ldisplay.setText(_fromUtf8(""))
        self.ldisplay.setObjectName(_fromUtf8("ldisplay"))
        self.bsumar = QtGui.QPushButton(Dialog)
        self.bsumar.setGeometry(QtCore.QRect(170, 180, 88, 27))
        self.bsumar.setObjectName(_fromUtf8("bsumar"))
        self.input1 = QtGui.QLineEdit(Dialog)
        self.input1.setGeometry(QtCore.QRect(170, 50, 113, 23))
        self.input1.setObjectName(_fromUtf8("input1"))
        self.input2 = QtGui.QLineEdit(Dialog)
        self.input2.setGeometry(QtCore.QRect(170, 110, 113, 23))
        self.input2.setObjectName(_fromUtf8("input2"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Casting numbers", None))
        self.label.setText(_translate("Dialog", "Primer numero", None))
        self.label_2.setText(_translate("Dialog", "Segundo numero", None))
        self.label_3.setText(_translate("Dialog", "Resultado", None))
        self.bsumar.setText(_translate("Dialog", "sumar", None))

