import sys
from bienvenida import *

class MiFormulario(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        QtCore.QObject.connect(self.ui.Button,QtCore.SIGNAL('clicked()'), self.mostrarmensaje)

    def mostrarmensaje(self):
        self.ui.labelmensaje.setText("Hola"+ self.ui.linename.text())


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = MiFormulario()
    myapp.show()
    sys.exit(app.exec_())
