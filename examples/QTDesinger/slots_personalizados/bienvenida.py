# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bienvenida.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(400, 300)
        self.labelname = QtGui.QLabel(Dialog)
        self.labelname.setGeometry(QtCore.QRect(20, 60, 121, 17))
        self.labelname.setObjectName(_fromUtf8("labelname"))
        self.labelmensaje = QtGui.QLabel(Dialog)
        self.labelmensaje.setGeometry(QtCore.QRect(160, 100, 64, 17))
        self.labelmensaje.setText(_fromUtf8(""))
        self.labelmensaje.setObjectName(_fromUtf8("labelmensaje"))
        self.linename = QtGui.QLineEdit(Dialog)
        self.linename.setGeometry(QtCore.QRect(160, 60, 113, 23))
        self.linename.setObjectName(_fromUtf8("linename"))
        self.Button = QtGui.QPushButton(Dialog)
        self.Button.setGeometry(QtCore.QRect(160, 130, 88, 27))
        self.Button.setObjectName(_fromUtf8("Button"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.labelname.setText(_translate("Dialog", "Escribe nombre", None))
        self.Button.setText(_translate("Dialog", "Pulsar", None))

