from __future__ import division #esto es para usar el operador de toda la vida
import sys
from buddy import *


class MiFormulario(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        QtCore.QObject.connect(self.ui.calcular, QtCore.SIGNAL('clicked()'), self.calcularm)

    def calcularm(self):
        if len(self.ui.cantidad.text())!= 0:
            q=int(self.ui.cantidad.text())
        else:
            q=0
        if len(self.ui.precio.text())!= 0:
            r=int(self.ui.precio.text())
        else:
            r=0
        if len(self.ui.descuento.text())!= 0:
            d=int(self.ui.descuento.text())
        else:
            d=0
        canttot = q*r
        desc = canttot * d/100
        cantnet = canttot - desc
        self.ui.ldisplay.setText("cantidad Total: " +str(canttot)+", Descuento: "+str(desc)+", Cantidad neta: "+str(cantnet))




if __name__ == '__main__':
   app = QtGui.QApplication(sys.argv) 
   myapp = MiFormulario()
   myapp.show()
   sys.exit(app.exec_())
