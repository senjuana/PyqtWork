# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'buddy.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(549, 300)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 50, 64, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(290, 50, 64, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.ldisplay = QtGui.QLabel(Dialog)
        self.ldisplay.setGeometry(QtCore.QRect(30, 200, 461, 20))
        self.ldisplay.setObjectName(_fromUtf8("ldisplay"))
        self.label_4 = QtGui.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(10, 110, 91, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.cantidad = QtGui.QLineEdit(Dialog)
        self.cantidad.setGeometry(QtCore.QRect(90, 50, 171, 23))
        self.cantidad.setObjectName(_fromUtf8("cantidad"))
        self.precio = QtGui.QLineEdit(Dialog)
        self.precio.setGeometry(QtCore.QRect(370, 50, 161, 23))
        self.precio.setObjectName(_fromUtf8("precio"))
        self.descuento = QtGui.QLineEdit(Dialog)
        self.descuento.setGeometry(QtCore.QRect(100, 110, 171, 23))
        self.descuento.setObjectName(_fromUtf8("descuento"))
        self.calcular = QtGui.QPushButton(Dialog)
        self.calcular.setGeometry(QtCore.QRect(260, 150, 88, 27))
        self.calcular.setObjectName(_fromUtf8("calcular"))
        self.label.setBuddy(self.cantidad)
        self.label_2.setBuddy(self.precio)
        self.label_4.setBuddy(self.descuento)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "buddies", None))
        self.label.setText(_translate("Dialog", "&Numero", None))
        self.label_2.setText(_translate("Dialog", "&Precio", None))
        self.ldisplay.setText(_translate("Dialog", "Resultado", None))
        self.label_4.setText(_translate("Dialog", "&Descuento", None))
        self.calcular.setText(_translate("Dialog", "Calcular", None))

