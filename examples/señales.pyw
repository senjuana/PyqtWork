import sys
from PyQt4 import QtCore,QtGui

class ventanaejemplo(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setGeometry(300,300,200,200)
        self.setWindowTitle('Ventana de ejemplo')
        quit = QtGui.QPushButton('Cerrar',self)
        quit.setGeometry(10,10,70,40)
        self.connect(quit, QtCore.SIGNAL('clicked()'), QtGui.qApp, QtCore.SLOT('quit()'))


app = QtGui.QApplication(sys.argv)
ve = ventanaejemplo()
ve.show()
sys.exit(app.exec_())
