import sys 
from PyQt4 import QtGui, QtCore

#esta clase es la ventana con pyqt 
class demowind(QtGui.QWidget):
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.setGeometry(300,300,200,200)
        self.setWindowTitle("Ventana de ejemplo")
        quit = QtGui.QPushButton('Close',self)
        quit.setGeometry(10,10,70,40)


# La instancia de la ventana
app = QtGui.QApplication(sys.argv)
dw = demowind()
dw.show()
sys.exit(app.exec_())
